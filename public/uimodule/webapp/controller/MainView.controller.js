sap.ui.define(
    ["./BaseController", "sap/ui/model/json/JSONModel"],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel) {
        "use strict";

        return Controller.extend("com.myorg.rtetohtml.controller.MainView", {
            onInit: function () {
                this.getView().setModel(new JSONModel({ text: "" }))

            },
            onBeforeEditorInit: function (event) {
                const config = event.getParameter("configuration");
                config.font_formats = "Arial=arial,helvetica,sans-serif;Calibri=Calibri, sans-serif;Times New Roman=times new roman,times;";
                config.toolbar = "fontselect fontsizeselect | bold italic underline strikethrough | forecolor | bullist numlist | align | link | removeformat | ";
                // https://www.tiny.cloud/docs/tinymce/6/content-filtering/#entity_encoding
                config.entity_encoding = "raw";
                config.setup = ed => {
                    ed.on('init', () => {
                        ed.getDoc().body.style.fontSize = '12';
                        ed.getDoc().body.style.fontFamily = 'Arial';
                    });
                };
            },
            escapeHTML: function (html) {
                return '"' + html.replace(/(?:\r\n|\r|\n)/g, '').replace(/"/g, '""') + '"';
            }
        });
    }
);
